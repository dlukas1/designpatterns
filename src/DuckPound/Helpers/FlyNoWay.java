package DuckPound.Helpers;

import DuckPound.Interfaces.FlyBehaviour;

public class FlyNoWay implements FlyBehaviour {

    public void fly(){
        System.out.println("I can't fly");
    }
}
