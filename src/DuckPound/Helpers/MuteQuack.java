package DuckPound.Helpers;

import DuckPound.Interfaces.QuackBehaviour;

public class MuteQuack implements QuackBehaviour {

    public void quack() {
        System.out.println("<<silence>>");
    }
}
