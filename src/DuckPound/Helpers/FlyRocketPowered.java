package DuckPound.Helpers;

import DuckPound.Interfaces.FlyBehaviour;

public class FlyRocketPowered implements FlyBehaviour {

    public void fly() {
        System.out.println("I'm flying with a rocket!");
    }
}
