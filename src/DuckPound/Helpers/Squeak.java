package DuckPound.Helpers;

import DuckPound.Interfaces.QuackBehaviour;

public class Squeak implements QuackBehaviour {

    public void quack() {
        System.out.println("Squeak!");
    }
}
