package DuckPound.Helpers;

import DuckPound.Interfaces.FlyBehaviour;

public class FlyWithWings implements FlyBehaviour {

    public void fly() {
        System.out.println("I'm flying on my wings!");
    }
}
