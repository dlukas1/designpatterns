package DuckPound;

import DuckPound.Helpers.FlyNoWay;
import DuckPound.Helpers.Quack;

public class ModelDuck extends Duck {

    public ModelDuck(){
        flyBehaviour = new FlyNoWay();
        quackBehaviour = new Quack();
    }



    public void display() {
        System.out.println("I'm a model duck");
    }
}
